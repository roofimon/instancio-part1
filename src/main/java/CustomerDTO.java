import java.util.List;
public record CustomerDTO(String name, String email, List<String> profiles) {}
