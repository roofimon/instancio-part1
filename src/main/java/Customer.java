import java.util.List;

public record Customer(String name, String email, List<Profile> profiles) {
}
