import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class CustomerMapper {
    public CustomerDTO toDto(Customer customer){
        String name = customer.name();
        String email = customer.email();
        List<String> profiles = customer.profiles().stream().map(Profile::profileName).collect(toList());
        return new CustomerDTO(name, email, profiles);
    }
    public Customer toCustomer(CustomerCreationDTO userDTO) {
        return new Customer(userDTO.name(), userDTO.email(), new ArrayList<>());
    }
}
