import java.util.List;

public record CustomerCreationDTO(String name, String email, List<String> profiles) { }
