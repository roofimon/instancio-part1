import org.instancio.Instancio;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.instancio.Select.field;

class CustomerTest {
    private CustomerMapper customerMapper;
    @BeforeEach
    void setUp() {
        customerMapper = new CustomerMapper();
    }

    @Test
    void verifyCustomerUsingInstancio() {
        Customer customer = Instancio.of(Customer.class)
                .generate(field("profiles"), gen -> gen.collection().size(4))
                .create();

        CustomerDTO customerDto = customerMapper.toDto(customer);

        assertThat(customerDto.name()).isEqualTo(customer.name());
        assertThat(customerDto.email()).isEqualTo(customer.email());
        assertThat(customerDto.profiles().size()).isEqualTo(4);
    }

    @Test
    void verifyCustomer() {
        Customer customer = new Customer("Somchai",
                "somchai@email.com",
                List.of(new Profile("departmentA"),
                        new Profile("departmentB")
                ));

        CustomerDTO customerDto = customerMapper.toDto(customer);

        assertThat(customerDto.name()).isEqualTo(customer.name());
        assertThat(customerDto.email()).isEqualTo(customer.email());
        assertThat(customerDto.profiles().size()).isEqualTo(2);
    }

    @AfterEach
    void tearDown() {
    }
}